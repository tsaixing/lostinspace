const canvas = document.querySelector('#canvas')
const width = window.innerWidth
const height = window.innerHeight

canvas.width = width
canvas.height = height

const ctx = canvas.getContext('2d')

const gutter = 20
const colCount = Math.ceil(width / gutter)
const rowCount = Math.ceil(height / gutter)

// ctx.font = '15px Helvetica'
ctx.fillStyle = 'white'

const drawCirc = (x,y = gutter,radius = 2) => {
  ctx.beginPath()
  ctx.arc(x,y,radius,0,2*Math.PI)
  ctx.fill()
}

const drawCircles = (mousePosition) => {
  const { x, y } = mousePosition

  _.times(rowCount, (index) => {
    const yOffset = index * gutter

    _.times(colCount, (index) => {
      const xOffset = index * gutter
      const maxDist = 80
      const maxRadius = 5
      /*
        TODO: change these distance calculations to be vectors instead of linear x and y.
        This would be done by calculating the the hypotenuse, eg Math.hypot((xOffset - x), (yOffset - y))
      */
      const xPosDist = (xOffset - x) < maxDist
      const xNegativeDist =  (x - xOffset) < maxDist

      const yPosDist = (yOffset - y) < maxDist
      const yNegativeDist =  (y - yOffset) < maxDist

      const xRadisuFalloff = (maxDist / (Math.abs(xOffset - x) + maxDist)) * maxRadius
      const yRadisuFalloff = (maxDist / (Math.abs(yOffset - y) + maxDist)) * maxRadius

      const radisuFalloff = [xRadisuFalloff, yRadisuFalloff].sort((a,b) => a - b)[0] // pick the smaller radius

      const radius = (xPosDist && xNegativeDist) && (yPosDist && yNegativeDist) ? radisuFalloff : 2
      drawCirc(xOffset, yOffset, radius)
    })
  })
}

drawCircles({x: 0, y: 0})

canvas.addEventListener('mousemove', (event) => {
  const x = event.clientX
  const y = event.clientY
  ctx.clearRect(0, 0, width, height)
  // ctx.fillText(`${x},${y}`,10,20)
  drawCircles({ x, y })
})