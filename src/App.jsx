import React, { Component } from "react";

import Starfield from "./components/Space/Starfield";

class App extends Component {
  componentDidMount() {
    document.title = "Lost In Space";
  }

  render() {
    return (
      <div id="space">
        <Starfield />
      </div>
    );
  }
}

export default App;
