  import React, { Component } from "react";

  require("./ship.css");

  class Ship extends Component {
    constructor(props) {
      super(props);
      this.state = {
        angle: -2.5, // Same direction as stars - northwest
        positionX: 0,
        positionY: 0,
        flameLength: 5,
        flameOpacity: 1
    }
  }

    render() {
      const containerStyle = {
        "position": "fixed",
        "z-index": "2000",
        "transform": "rotate(" + this.state.angle + "rad)"
      };
      const flameStyle = {
        "margin-bottom": this.state.flameLength + "rem",
        "opacity": this.state.flameOpacity
      };

      return (
        <div style={containerStyle}>
          <img
            src={require("../svg/ship.svg")}
            alt="Dinoship"
            id="ship"
            height="80"
          />
          <div id="engine">
            <div class="flame" style={flameStyle} />
            <div class="flame" style={flameStyle} />
            <div class="flame" style={flameStyle} />
            <div class="flame" style={flameStyle} />
            <div class="flame" style={flameStyle} />
          </div>
        </div>
      );
    }
  }

  export default Ship;
