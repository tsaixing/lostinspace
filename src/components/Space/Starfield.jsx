import React, { Component } from "react";
import Star from "./Star";
import Ship from "./Ship";

class Starfield extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth,
      height: window.innerHeight,
      ratio: window.devicePixelRatio || 1,
      mouseX: 0,
      mouseY: 0
    };
    this.fps = 60;
    this.canvas = null;
    this.minVelocity = 10;
    this.maxVelocity = 80;
    this.layers = 3;
    this.starsCount = 100;
    this.intervalID = 0;

    this.shipRef = React.createRef();
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount = () => {
    window.addEventListener("resize", this.updateDimensions);

    this.canvas = document.getElementById("canvas");
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.initStars();
    this.draw();
    this.setInterval();
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);

    clearInterval(this.intervalID);
  }

  setInterval() {
    this.intervalID = setInterval(() => {
      // this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.updateStars();
      this.draw();
    }, 1000 / this.fps);
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight,
      ratio: window.devicePixelRatio || 1
    });

    if (this.canvas != null) {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
    }
  };

  _onMouseMove(e) {
    this.setState({
      mouseX: e.clientX,
      mouseY: e.clientY
    });

    const originX = window.innerWidth / 2;
    const originY = window.innerHeight / 2;
    const radian = Math.atan2(e.clientY - originY, e.clientX - originX);

    const maxFlameLength = 5;
    const maxFlameOpacity = 1;
    const deltaX = Math.abs(originX - e.clientX);
    const deltaY = Math.abs(originY - e.clientY);

    const powerX = (deltaX * maxFlameLength) / originX;
    const powerY = (deltaY * maxFlameLength) / originY;
    const power = Math.max(powerX, powerY);
    const opacityX = (deltaX * maxFlameOpacity) / originX;
    const opacityY = (deltaY * maxFlameOpacity) / originY;
    const opacity = Math.max(opacityX, opacityY);

    this.shipRef.current.setState({
      angle: radian,
      flameLength: power,
      flameOpacity: opacity
    });
  }

  initStars() {
    var stars = [];

    for (var i = 0; i < this.starsCount; i++) {
      stars[i] = new Star({
        x: Math.floor(Math.random() * this.state.width),
        y: Math.floor(Math.random() * this.state.height),
        size: Math.floor(Math.random() * 5),
        velocity:
          Math.random() * (this.maxVelocity - this.minVelocity) +
          this.minVelocity
      });
    }

    this.stars = stars;
  }

  updateStars() {
    const canvasWidth = this.state.width;
    const canvasHeight = this.state.height;
    const rate = 1 / this.fps;

    // Calculate star coordinates based on mouse positioning
    // Note: Mouse position (0,0) is located on the top left corner
    const buffer = 20;
    const mouseX = this.state.mouseX;
    const mouseY = this.state.mouseY;
    const centerX = Math.floor(canvasWidth / 2);
    const centerY = Math.floor(canvasHeight / 2);
    const modifierX = Math.abs(Math.floor(centerX - mouseX) * 0.002);
    const modifierY = Math.abs(Math.floor(centerY - mouseY) * 0.002);

    for (var i = 0; i < this.starsCount; i++) {
      var star = this.stars[i];

      // Horizontal positioning
      if (mouseX < centerX - buffer) {
        star.props.x += rate * (star.props.velocity * modifierX);
      }
      if (mouseX > centerX + buffer) {
        star.props.x -= rate * (star.props.velocity * modifierX);
      }

      // Vertical positioning
      if (mouseY < centerY - buffer) {
        star.props.y += rate * (star.props.velocity * modifierY);
      }
      if (mouseY > centerY + buffer) {
        star.props.y -= rate * (star.props.velocity * modifierY);
      }
    }
  }

  //  Spawn new stars when existing stars reach the canvas edge
  keepInView(star, i) {
    const canvasWidth = this.state.width;
    const canvasHeight = this.state.height;

    // Horizontal movement
    if (star.props.x < 0) {
      this.stars[i] = new Star({
        x: canvasWidth,
        y: Math.floor(Math.random() * canvasHeight),
        size: Math.floor(Math.random() * 5),
        velocity:
          Math.random() * (this.maxVelocity - this.minVelocity) +
          this.minVelocity
      });
    }
    if (star.props.x > canvasWidth) {
      this.stars[i] = new Star({
        x: 0,
        y: Math.floor(Math.random() * canvasHeight),
        size: Math.floor(Math.random() * 5),
        velocity:
          Math.random() * (this.maxVelocity - this.minVelocity) +
          this.minVelocity
      });
    }
    // Vertical movement
    if (star.props.y < 0) {
      this.stars[i] = new Star({
        x: Math.floor(Math.random() * canvasWidth),
        y: canvasHeight,
        size: Math.floor(Math.random() * 5),
        velocity:
          Math.random() * (this.maxVelocity - this.minVelocity) +
          this.minVelocity
      });
    }
    if (star.props.y > canvasHeight) {
      this.stars[i] = new Star({
        x: Math.floor(Math.random() * canvasWidth),
        y: 0,
        size: Math.floor(Math.random() * 5),
        velocity:
          Math.random() * (this.maxVelocity - this.minVelocity) +
          this.minVelocity
      });
    }
  }

  draw() {
    const context = this.canvas.getContext("2d");
    const canvasWidth = this.state.width;
    const canvasHeight = this.state.height;

    // Draw background
    // context.fillStyle = "rgba(93, 97, 127,1)";
    // context.fillStyle = 'rgba(60, 50, 80, 1)';
    context.fillStyle = "rgba(64,47,63,1)";
    // context.fillStyle = "rgba(42,51,80,1)";
    context.fillRect(0, 0, canvasWidth, canvasHeight);

    // Draw Stars
    for (var i = 0; i < this.starsCount; i++) {
      var star = this.stars[i];

      context.beginPath();
      context.arc(
        star.props.x,
        star.props.y,
        star.props.size,
        Math.PI * 2,
        0,
        false
      );
      context.fillStyle = "rgba(255,255,255,1)";
      context.fill();
      context.closePath();

      this.keepInView(star, i);
    }
  }

  render() {
    const starfieldStyle = {
      display: "flex",
      "align-items": "center",
      "justify-content": "center"
    };
    // const { mouseX, mouseY } = this.state;

    return (
      <div
        onMouseMove={this._onMouseMove.bind(this)}
        onMouseDown={this._onMouseMove.bind(this)}
        style={starfieldStyle}
      >
        {/* <h1>Mouse coordinates: {mouseX} {mouseY}</h1> */}
        {/* {<h1>Left buffer: {centerX-10} Right buffer: {}</h1>} */}
        <Ship ref={this.shipRef} />
        <canvas id="canvas" className="canvas" ref="canvas" />
      </div>
    );
  }
}

export default Starfield;
